﻿; AutoHotKey script to add German letter key shortcuts

;---------------------------------------------
; ^ -> Ctrl key
; ! -> Alt key
; + -> Shift key
; :: denotes what you want the preceding keys to run when pressed together
; Send, is a command that types the proceeding text
;---------------------------------------------

; ALT+A -> ä
!a:: Send, ä

; ALT+O -> ö
!o:: Send, ö

; ALT+U -> ü
!u:: Send, ü

; ALT+SHIFT+A -> Ä
!+a:: Send, Ä

; ALT+SHIFT+O -> Ö
!+o:: Send, Ö

; ALT+SHIFT+U -> Ü
!+u:: Send, Ü

; ALT+S -> ß (Eszett)
!s:: Send, ß
